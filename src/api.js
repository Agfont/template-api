const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");

const validator = require("./validator");

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));

  /* ******************* */
  /* YOUR CODE GOES HERE */
  /* ******************* */
  const db = mongoClient.db('user-db');
  
  /* Create a User */
  api.post("/users", async (req, res) => {
    const params = req.body;
    const id = uuid();

    const missingParam = validator.areMissingFields(params);
    if (missingParam) {
      return res.status(400).json({ 
        error: "Request body had missing field ${missingParam}" });
    }

    const malformedParam = validator.areMalformedFields(params);
    if (malformedParam) {
      return res.status(400).json({ 
        error: "Request body had malformed field ${malformedParam}" });
    }

    const userAlreadyExist = await db.collection('users').findOne({ email: params.email });
    if (userAlreadyExist) {
      return res.status(400).json({ 
        error: "Request body had malformed field email (User already exist)" });
    }

    if (!validator.passwordsMatches(params)) {
      return res.status(422).json({ 
        error: "Password confirmation did not match" });
    }
    /*
    await db.collection('users').insertOne({
      id: id,
      name: params.name,
      email: params.email,
      password: params.password,
    })*/

    stanConn.publish('users', JSON.stringify({
      eventType: "UserCreated",
      entityId: id,
      entityAggregate: {
        name: params.name,
        email: params.email,
        password: params.password,
      },
    }));
  
    res.status(201).json({
      user: {
        id: id,
        name: params.name,
        email: params.email
      }
    });
  })

  /* Delete a User */
  api.delete("/users/:uuid", async (req, res) => {
    const uuid = req.params.uuid;
    const auth = req.get("Authentication");

    if (!auth) {
      return res.status(401).json({ 
        error: "Access Token not found" });
    }

    const decodedToken = jwt.verify(auth.split(' ')[1], secret);

    if (uuid !== decodedToken.id) {
      return res.status(403).json({ 
        error: "Access Token did not match User ID" });
    }

    const query = await db.collection('users').findOne({ id: uuid})
    //if (query) await db.collection('users').deleteOne({ id: uuid })

    stanConn.publish("users", JSON.stringify({
      eventType : "UserDeleted",
      entityId : uuid,
      entityAggregate : {},
    }));

    res.status(200).json({ 
      id : uuid});
  })

  return api;
};