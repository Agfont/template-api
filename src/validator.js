function areMissingFields (params) {
    const requiredFields = ['name', 'email', 'password', 'passwordConfirmation'];
    let missingField = null;

    if (!params.name) missingField = "name";
    else if (!params.email) missingField = "email";
    else if (!params.password) missingField = "password";
    else if (!params.passwordConfirmation) missingField = "passwordConfirmation"

    return missingField;
}

function areMalformedFields (params) {
    const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*/;
    const passwordRegex = /\b[a-zA-Z0-9]{8,32}\b/;
    let malformedField = null;

    if (!emailRegex.test(params.email)) malformedField = 'email';
    if (!passwordRegex.test(params.password)) malformedField = 'password';

    return malformedField;
}

function passwordsMatches (params) {
    return params.password === params.passwordConfirmation;
}

module.exports = {areMissingFields, areMalformedFields, passwordsMatches};